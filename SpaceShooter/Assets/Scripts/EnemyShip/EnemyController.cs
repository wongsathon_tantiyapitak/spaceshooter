﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;

        private PlayerSpaceship spawnPlayerShip;

        //public void Init(PlayerSpaceship playerSpaceship)
        //{
         //   spawnPlayerShip = playerSpaceship;
        //}
        private void Awake()
        {
            spawnPlayerShip = GameManager.Instance.spawnedPlayerShip;
        }
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
             var distanceToPlayer = Vector2.Distance(spawnPlayerShip.transform.position, transform.position);
             Debug.Log(distanceToPlayer);
             if (distanceToPlayer < chasingThresholdDistance)
             {
                 var direction = (Vector2)(spawnPlayerShip.transform.position - transform.position);
                 direction.Normalize();
                 var distance = direction * enemySpaceship.Speed * Time.deltaTime;
                 gameObject.transform.Translate(distance);
             }
         }
    }    
}

