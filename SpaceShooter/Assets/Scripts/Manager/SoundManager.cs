﻿using System;
using UnityEngine;
using UnityEngine.Video;

namespace Assets.Scripts.Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        
        public static SoundManager Instance { get; private set; }

        private AudioSource audioSource;
        public void Awake()
        {
            Debug.Assert(soundClips != null && soundClips.Length != 0,"Sound clips need to be setup");
            audioSource = GetComponent<AudioSource>();

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }
        public enum Sound
        {
            BGM,
            PlayerFire,
            PlayerExplosion,
            EnemyFire,
            EnemyExplosion
        }
        
        [Serializable]
        public struct SoundClip
        {
            public Sound Sound;
            public AudioClip AudioClip;
            [Range(0,1)] public float SoundVolume;
        }

        public void Play(AudioSource audioSource, Sound sound)
        {
            var soundClip = GetSoundClip(sound);
            audioSource.clip = soundClip.AudioClip;
            audioSource.volume = soundClip.SoundVolume;
            audioSource.Play();
        }

        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(audioSource,Sound.BGM);
        }
        private SoundClip GetSoundClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip;
                }   
            }

            return default;
        }
    }
}
