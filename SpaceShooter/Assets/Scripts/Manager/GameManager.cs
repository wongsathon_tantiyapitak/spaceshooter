﻿using System;
using Assets.Scripts.Manager;
using Enemy;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        //[SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        public PlayerSpaceship spawnedPlayerShip;
        //[SerializeField] private SoundManager soundManager;
        public static GameManager Instance { get; private set; }
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            //Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            //soundManager.PlayBGM();
            SoundManager.Instance.PlayBGM();
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }
        
            
        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void StartGame()
        {
            ScoreManager.Instance.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spawnEnemyship = Instantiate(enemySpaceship);
            spawnEnemyship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnEnemyship.OnExploded += OnEnemySpaceshipExploded;

            //var enemyController = spawnEnemyship.GetComponent<EnemyController>();
            //enemyController.Init(spawnedPlayerShip);
        }

        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.SetScore(1);
            Restart();
        }

        private void Restart()
        {
            DestroyRemainingObject();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingObject()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }

            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }
    }
}
