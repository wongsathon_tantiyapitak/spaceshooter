﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    private float time;
    [SerializeField] private TextMeshPro remainingTime;
    void Start()
    {
        remainingTime = this.GetComponent<TextMeshPro>();
    }

    
    void Update()
    {
        while (time > 0)
        {
            time -= Time.deltaTime;
            remainingTime.text = $"Time : {time}";
        }
    }
}
